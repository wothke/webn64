# webN64

Copyright (C) 2018 Juergen Wothke

This is a JavaScript/WebAudio plugin of kode54's "LazyUsf2 - USF Decoder" (based on Mupen64plus). This 
plugin is designed to work with my generic WebAudio ScriptProcessor music player (see separate project). 

It allows to play "(Ultra) Nintendo 64 Sound" music (.USF/.MINIUSF2 files).

A live demo of this program can be found here: http://www.wothke.ch/webn64/


update (march 2023): I updated the code to kode54's latest version.


## Credits
The project is based on: https://gitlab.kode54.net/kode54/lazyusf2 


## Project
It includes most of the original "USF Decoder" code including all the necessary 3rd party dependencies. All the "Web" specific 
additions (i.e. the whole point of this project) are contained in the "emscripten" subfolder. Due to the unavailability of SSE2 
and x86 assembly code support in EMSCRIPTEN the "cached interpreter" mode is used.) The main interface 
between the JavaScript/WebAudio world and the original C code is the adapter.c file. Some patches were necessary 
within the original codebase (these can be located by looking for EMSCRIPTEN if-defs). 


## Howto build

You'll need Emscripten (http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html). The make script 
is designed for use of emscripten version 1.37.29 (unless you want to create WebAssembly output, older versions might 
also still work). Note: The most recent Emscripten version that I "successfully" built this with is 1.38.13. I also 
tried to use Emscripten 3.1.x but for some reason (that I do not care to waste my time on researching) the built code 
becomes unusable garbage, i.e. it will crash while trying to allocate insane amounts of memory (the PSF loading logic 
seems to get utterly corrupted; this might be an indication that the PSF/loader logic is actually flawed - or maybe the culprit 
is actually the new Emscripten toolchain).


The below instructions assume that the webn64 project folder has been moved into the main emscripten 
installation folder (maybe not necessary) and that a command prompt has been opened within the 
project's "emscripten" sub-folder, and that the Emscripten environment vars have been previously 
set (run emsdk_env.bat).

The Web version is then built using the makeEmscripten.bat that can be found in this folder. The 
script will compile directly into the "emscripten/htdocs" example web folder, were it will create 
the backend_n64.js library. (To create a clean-build you have to delete any previously built libs in the 
'built' sub-folder!) The content of the "htdocs" can be tested by first copying it into some 
document folder of a web server. 


## Depencencies

The current version requires version >=1.2.1 of my https://bitbucket.org/wothke/webaudio-player/

This project comes without any music files, so you'll also have to get your own and place them
in the htdocs/music folder (you can configure them in the 'songs' list in index.html).


## Disclaimer

This emulator is not as stable as I would like and there are songs which actually manage to crash 
it. From what I've read there does not seem to be one perfect Nintendo64 emulator and what works with 
one may fail with the next (and vice versa). And who knows, maybe future improvements will make this 
one more stable.

Performance wise this emulator is still testing the limits of what is just barely fast enough to run
in a browser, i.e. it may run OK on one machine but not on a different one. When I first ran it 
on my older "AMD bulldozer" based CPU some songs had worked fine - and 5 years later these 
same songs now stutter on my newer (still old) Intel i7 CPU. It is unclear to what degree those 
performance degradations may be the fault of the used CPU, changes in browers implementations or changes
in the Emscripten toolchain. 


## License

This library is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or (at
your option) any later version. This library is distributed in the hope
that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

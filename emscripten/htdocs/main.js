let songs = [
		"Ultra64 Sound Format/- unknown/Wipeout 64/05. absurd.miniusf",
		"Ultra64 Sound Format/- unknown/Wipeout 64/09. sparse08 (tomorrow reborn reprise).miniusf",
		"Ultra64 Sound Format/- unknown/Wipeout 64/08. credits (miles ahead).miniusf",
	];

class N64DisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "webN64";}
	getDisplaySubtitle() 	{ return "LazyUSF music..";}
	getDisplayLine1() { return this.getSongInfo().title +" ("+this.getSongInfo().artist+")";}
	getDisplayLine2() { return this.getSongInfo().copyright; }
	getDisplayLine3() { return ""; }
};

class N64Controls extends BasicControls {
	constructor(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current)
	{
		super(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current);
	}

	_addSong(filename)
	{
		if (filename.indexOf(".usflib") == -1)
		{
			super._addSong(filename);
		}
	}
};

class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new N64BackendAdapter();
//		this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new N64Controls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new N64DisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x5970e2,0x25db26,0xdcdc2e,0xdcdc2e,0xd12525,0x25db26,0x5970e2,0x25db26,0xdcdc2e,0xdcdc2e,0xd12525,0x25db26], 1, 0.1);

			this._playerWidget.playNextSong();
		});
	}
}
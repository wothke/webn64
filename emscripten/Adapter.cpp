/*
* This file adapts "LazyUSF2" to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2018-2023 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

// problem: it seems _lib files here may again recursively refer to other _lib files
// e.g. Ultra64 Sound Format/Yasunori Mitsuda/Mario Party/mp1_1.usflib -> mp1rom.usflib

#include <stdio.h>
#include <stdlib.h>     /* malloc, free, rand */
#include <exception>
#include <iostream>
#include <fstream>

#include <emscripten.h>

#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;


// interface to n64plug.cpp
extern	void n64_setup (void);
extern	void n64_boost_volume(unsigned char b);
extern	int32_t n64_get_samples_to_play ();
extern	int32_t n64_get_samples_played ();
extern	int32_t n64_get_sample_rate ();
extern	int n64_load_file(const char *uri, int16_t *output_buffer, uint16_t outSize);
extern	int n64_read(int16_t *output_buffer, uint16_t outSize);
extern	int n64_seek_sample (int sampleTime);

void n64_meta_set(const char * tag, const char * value)
{
	MetaInfoHelper *info = MetaInfoHelper::getInstance();
	// propagate selected meta info for use in GUI
	if (!strcasecmp(tag, "title"))
	{
		info->setText(0, value, "");
	}
	else if (!strcasecmp(tag, "artist"))
	{
		info->setText(1, value, "");
	}
	else if (!strcasecmp(tag, "album"))
	{
		info->setText(2, value, "");
	}
	else if (!strcasecmp(tag, "date"))
	{
		info->setText(3, value, "");
	}
	else if (!strcasecmp(tag, "genre"))
	{
		info->setText(4, value, "");
	}
	else if (!strcasecmp(tag, "copyright"))
	{
		info->setText(5, value, "");
	}
	else if (!strcasecmp(tag, "usfby"))
	{
		info->setText(6, value, "");
	}
}

#define CHANNELS 2
#define BYTES_PER_SAMPLE 2
#define SAMPLE_BUF_SIZE	1024

namespace n64 {

	class Adapter {
	private:
		int16_t _sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];
		int _samplesAvailable;
	public:
		Adapter() : _samplesAvailable(0)
		{
		}

		int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: sampleRate, audioBufSize, scopesEnabled support not implemented (much is hardcoded in the DS emulator)
			n64_setup();
			return (n64_load_file(filename, _sampleBuffer, SAMPLE_BUF_SIZE) == 0) ? 0 : -1;
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();
		}

		int getSampleRate()
		{
			return n64_get_sample_rate();
		}

		int setBoost(unsigned char boost)
		{
			n64_boost_volume(boost);
			return 0;
		}

		char* getAudioBuffer()
		{
			return (char*)_sampleBuffer;
		}

		long getAudioBufferLength()
		{
			return _samplesAvailable;
		}

		int getCurrentPosition()
		{
			return ((long long)n64_get_samples_played()) * 1000 /  n64_get_sample_rate();
		}

		int getMaxPosition()
		{
			return ((long long)n64_get_samples_to_play()) * 1000 /  n64_get_sample_rate();
		}

		void seekPosition(int pos) {
			n64_seek_sample(((long long)pos) * n64_get_sample_rate() / 1000);
		}

		int genSamples()
		{
			int ret =  n64_read((short*)_sampleBuffer, SAMPLE_BUF_SIZE);	// returns number of bytes

			if (ret < 0)
			{
				_samplesAvailable = 0;
				return 1;
			}
			else
			{
				_samplesAvailable = ret; // available time (measured in samples)

				return ret ? 0 : 1;
			}
		}
	};
}

static n64::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown()) 									{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate()) 								{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info()) 						{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(char*, emu_get_audio_buffer()) 							{ return _adapter.getAudioBuffer(); }
EMBIND(long, emu_get_audio_buffer_length()) 					{ return _adapter.getAudioBufferLength(); }
EMBIND(int, emu_compute_audio_samples()) 						{ return _adapter.genSamples(); }
EMBIND(int, emu_get_current_position()) 						{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int pos)) 						{ _adapter.seekPosition(pos); }
EMBIND(int, emu_get_max_position()) 							{ return _adapter.getMaxPosition(); }

// --- add-on
EMBIND(int, emu_set_boost(int boost))				{ return _adapter.setBoost(boost); }
